# Zeugnis-Seite

## Changelog

### 2024-06-18

Im gleichen Toolbar jetzt auch Export Kommentare als .docx-Datei. Kommentare
sind pro Periode getrennt, Noten nicht. Die Namen der heruntergeladenen Dateien
sind jetzt einfacher.

### 2024-06-17

The generated csv file has now delimiter ";" instead of "," and decimal
separator is a comma (",") instead of a dot (".").

### 2024-06-14

Erster Vorschlag für #5653 (Export der Punkte pro Klasse): in der Klassenseite
gibt es jetzt einen neuen Link "Noten exportieren". Den Link "Lehrerrollen
bearbeiten", der bisher hinterm Titel stand, habe ich unter den Titel verschoben
und das Layout optimiert. Statt eines Riesenbuttons "Zeugnisse ausfüllen" gibt
es jetzt eine "Toolbar" mit den drei "Aktionen", die man inzwischen von einer
Klasse aus starten kann.

  Lehrerrollen bearbeiten | Zeugnisse ausfüllen | Noten exportieren

## Copyright

See files COPYING and LICENSE
