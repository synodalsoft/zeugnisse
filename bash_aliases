# Common bash aliases use by the Synodalsoft team on Lino servers
# Every user may add this to their .bashrc:
# . /usr/local/pdg2/bash_aliases

alias a='. env/bin/activate'
alias ll='ls -alF'
alias pm='python manage.py'
alias runserver='LINO_LOGLEVEL=DEBUG python manage.py runserver'
function pywhich() {
  python -c "import $1; print($1.__file__)"
}

function go() {
    for BASE in /usr/local/synodal
    do
      if [ -d $BASE/$1 ] ; then
        cd $BASE/$1;
        return;
      fi
    done
    echo Oops: no project $1
    return -1
}
