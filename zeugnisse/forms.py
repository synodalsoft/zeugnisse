from collections import defaultdict
from decimal import Decimal

from django import core, forms
from django.forms import (BaseInlineFormSet, CharField, ChoiceField,
                          DecimalField, Form, IntegerField, ModelForm,
                          NumberInput, Textarea, inlineformset_factory)

from .models import (PRÄDIKATE, ZEICHEN, Baustein, BausteinAbschnitt,
                     BausteinArbeit, BausteinArbeitBewertung,
                     BausteinKommentar, Leistung, LeistungsBewertung, Test, TestKommentar,
                     Zeugnis, ZeugnisFachKommentar, ZeugnisKompetenzNote,
                     ZeugnisPrädikatNote, ZeugnisSozialNote, aktuelle_periode)

from zeugnisse.logic import anteil, gesamtpunkte_pro_kompetenz, berechne_note, prozentsatz


class TestFieldForm(Form):
    def __init__(self, *args, label, max_punkte, **kwargs):
        super(TestFieldForm, self).__init__(*args, **kwargs)
        self.fields["punkte"] = DecimalField(
            min_value=0,
            max_value=max_punkte,
            initial="",
            label=label,
            label_suffix=label,
            required=False,
            localize=True,
            widget=NumberInput(
                attrs={
                    'class': 'input is-radiusless is-shadowless is-medium',
                    'type': 'number',
                    'min': 0,
                    'max': max_punkte,
                    'step': 0.1,
                })
        )


class TestKommentarForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TestKommentarForm, self).__init__(*args, **kwargs)
        self.fields['kommentar'].required = False
        self.fields['kommentar'].widget.attrs.update({
            'class': 'input textarea is-radiusless is-shadowless',
            'rows': 4
        })

    class Meta:
        model = TestKommentar
        fields = ('kommentar',)


class BausteinAbschnittBewertungForm(Form):
    error_css_class = "is-danger"

    def __init__(self, request, abschnitt, *args, **kwargs):
        super(BausteinAbschnittBewertungForm, self).__init__(
            request, *args, **kwargs)
        self.bewertungen = dict()
        for leistung in abschnitt.leistung_set.all():
            bewertung = leistung.vlt_bewertung[0] if leistung.vlt_bewertung else None
            self.bewertungen[leistung.id] = bewertung
            self.fields["b:"+str(leistung.id)] = DecimalField(
                min_value=0,
                max_value=leistung.max_punkte,
                initial=str(bewertung.punkte) if bewertung else None,
                label=str(leistung.kompetenz),
                label_suffix="",
                required=False,
                localize=True,
                widget=forms.NumberInput(
                    attrs={
                        'class': 'input is-radiusless is-shadowless is-medium',
                        'type': 'number',
                        'min': 0,
                        'max': leistung.max_punkte,
                        'step': 0.1,
                    })
            )
            self.fields["p:"+str(leistung.id)] = ChoiceField(
                initial=bewertung.periode if bewertung else aktuelle_periode(),
                choices=((1, "1"), (2, "2")),
                required=True,
                localize=True,
            )

            self.fields["b:"+str(leistung.id)].leistung = leistung
            self.fields["p:"+str(leistung.id)].leistung = leistung

    def save(self, lehrer, schüler, datum):
        changed_items = set()
        for key in self.changed_data:
            leistung_id = int(key[2:])
            changed_items.add(leistung_id)

        for leistung_id in changed_items:
            bewertung = self.bewertungen.get(leistung_id)
            if bewertung:
                bewertung.delete()

            punkte = self.cleaned_data["b:{}".format(leistung_id)]
            periode = self.cleaned_data["p:{}".format(leistung_id)]
            if punkte is not None:
                bewertung = LeistungsBewertung.objects.create(
                    punkte=punkte,
                    periode=periode,
                    lehrer=lehrer,
                    schüler=schüler,
                    datum=datum,
                    leistung_id=leistung_id,
                ).save()

    def calculate_sum(self):
        self.gesamtpunkte = [Decimal(0), Decimal(0)]
        for key, value in self.cleaned_data.items():
            if key.startswith("b:") and self.cleaned_data[key] is not None:
                self.gesamtpunkte[0] += self.cleaned_data[key]
                self.gesamtpunkte[1] += self.fields[key].max_value

        if self.gesamtpunkte[1] > 0:
            self.prozentsatz = prozentsatz(
                self.gesamtpunkte[0], self.gesamtpunkte[1])


class BausteinArbeitBewertungForm(Form):
    error_css_class = "is-danger"

    def __init__(self, request, baustein, schüler, *args, **kwargs):
        super(BausteinArbeitBewertungForm, self).__init__(
            request, *args, **kwargs)
        self.bewertungen = dict()
        self.baustein = baustein
        for arbeit in BausteinArbeit.objects.all():
            try:
                bewertung = BausteinArbeitBewertung.objects.get(
                    schüler=schüler, arbeit=arbeit, baustein=baustein)
            except BausteinArbeitBewertung.DoesNotExist:
                bewertung = None
            self.bewertungen[arbeit.id] = bewertung
            self.fields["b:"+str(arbeit.id)] = DecimalField(
                min_value=0,
                max_value=arbeit.max_punkte,
                initial=str(bewertung.punkte) if bewertung else None,
                label=str(arbeit.name),
                label_suffix="",
                required=False,
                localize=True,
                widget=forms.NumberInput(
                    attrs={
                        'class': 'input is-radiusless is-shadowless is-medium',
                        'type': 'number',
                        'min': 0,
                        'max': arbeit.max_punkte,
                        'step': 0.5,
                    })
            )
            self.fields["p:"+str(arbeit.id)] = ChoiceField(
                initial=bewertung.periode if bewertung else aktuelle_periode(),
                choices=((1, "1"), (2, "2")),
                required=True,
                localize=True,
            )
            self.fields["b:"+str(arbeit.id)].arbeit = arbeit
            self.fields["p:"+str(arbeit.id)].arbeit = arbeit

    def save(self, lehrer, schüler, datum):
        changed_items = set()
        for key in self.changed_data:
            arbeit_id = int(key[2:])
            changed_items.add(arbeit_id)

        for arbeit_id in changed_items:
            bewertung = self.bewertungen.get(arbeit_id)
            if bewertung:
                bewertung.delete()

            punkte = self.cleaned_data["b:{}".format(arbeit_id)]
            periode = self.cleaned_data["p:{}".format(arbeit_id)]
            if punkte is not None:
                bewertung = BausteinArbeitBewertung.objects.create(
                    punkte=punkte,
                    periode=periode,
                    lehrer=lehrer,
                    schüler=schüler,
                    datum=datum,
                    baustein=self.baustein,
                    arbeit_id=arbeit_id,
                ).save()

    def calculate_sum(self):
        self.gesamtpunkte = [Decimal(0), Decimal(0)]
        for key, value in self.cleaned_data.items():
            if key.startswith("b:") and self.cleaned_data[key] is not None:
                self.gesamtpunkte[0] += self.cleaned_data[key]
                self.gesamtpunkte[1] += self.fields[key].max_value

        if self.gesamtpunkte[1] > 0:
            self.prozentsatz = prozentsatz(
                self.gesamtpunkte[0], self.gesamtpunkte[1])


class BausteinKommentarForm(ModelForm):
    prefix = "kommentar"

    def __init__(self, *args, **kwargs):
        super(BausteinKommentarForm, self).__init__(*args, **kwargs)
        self.fields['kommentar'].required = False

    class Meta:
        model = BausteinKommentar
        fields = ('kommentar',)


class ZeugnisForm(ModelForm):
    error_css_class = "is-danger"

    def __init__(self, request, instance=None, prefix=""):
        super(ZeugnisForm, self).__init__(
            request, instance=instance, prefix=prefix)
        choices = self['katholische_religion'].field.widget.choices
        choices += [(True, instance.schüler.klasse.ka_religionslehrerin),
                    (False, instance.schüler.klasse.ev_religionslehrerin)]

    class Meta:
        model = Zeugnis
        fields = ('status', 'katholische_religion', 'sozialverhalten_kommentar', 'versetzungsentscheidung',
                  'abwesenheiten_mit_entschuldigung', 'abwesenheiten_mit_attest', 'abwesenheiten_ohne', 'förderziele')
        widgets = {
            'katholische_religion': forms.Select(),
            'sozialverhalten_kommentar': Textarea(
                attrs={
                    'class': 'input textarea',
                    'rows': '20',
                }
            ),
            'versetzungsentscheidung': Textarea(
                attrs={
                    'class': 'input textarea',
                    'rows': '20',
                }
            ),
            'abwesenheiten_mit_entschuldigung': forms.NumberInput(
                attrs={
                    'class': 'input',
                    'type': 'number',
                            'min': 0,
                            'max': 999,
                            'step': 1,
                }),
            'abwesenheiten_mit_attest': forms.NumberInput(
                attrs={
                    'class': 'input',
                    'type': 'number',
                            'min': 0,
                            'max': 999,
                            'step': 1,
                }),
            'abwesenheiten_ohne': forms.NumberInput(
                attrs={
                    'class': 'input',
                    'type': 'number',
                            'min': 0,
                            'max': 999,
                            'step': 1,
                }),
            'förderziele': Textarea(
                attrs={
                    'class': 'input textarea',
                    'rows': '20',
                }
            ),
        }


class ZeugnisAbschnittForm(Form):
    error_css_class = "is-danger"

    def __init__(self, request, abschnitt, zeugnis, gesamtpunkte,
                 abschlussbewertungen, *args, **kwargs):
        super(ZeugnisAbschnittForm, self).__init__(
            request, *args, **kwargs)
        self.zeugnis = zeugnis
        self.abschnitt = abschnitt
        if abschnitt.fach.advanced:
            self.overrides = dict()
            for element in abschnitt.elemente:
                override = element.vlt_kompetenznote[0] if element.vlt_kompetenznote else None
                self.overrides[element.id] = override
                berechnet = berechne_note(
                    element, zeugnis.periode, gesamtpunkte, abschlussbewertungen)
                self.fields["element:{}".format(element.id)] = DecimalField(
                    min_value=0,
                    max_value=element.max_punkte,
                    initial=str(override.note) if override else "",
                    label=element.kompetenz.beschreibung,
                    label_suffix=berechnet,
                    required=False,
                    localize=True,
                    widget=forms.NumberInput(
                        attrs={
                            'class': 'input',
                            'type': 'number',
                            'min': 0,
                            'max': element.max_punkte,
                            'step': 0.1,
                        })
                )
        else:
            self.prädikatnote = abschnitt.vlt_prädikatnote[0] if abschnitt.vlt_prädikatnote else None
            self.fields["note:0"] = ChoiceField(
                choices=((None, ''),) + PRÄDIKATE,
                label="Note",
                required=False,
                initial=self.prädikatnote.note if self.prädikatnote else None,
            )

        self.kommentar = abschnitt.vlt_fachkommentar[0] if abschnitt.vlt_fachkommentar else None
        self.fields['kommentar:0'] = CharField(
            initial=self.kommentar.kommentar if self.kommentar else "",
            label="Kommentar",
            widget=Textarea(
                attrs={
                    'class': 'input textarea',
                }
            ),
            required=False,
        )

    def save(self):
        schüler = self.zeugnis.schüler
        periode = self.zeugnis.periode
        for key in self.changed_data:
            [kind, index] = key.split(":")
            if kind == "element":
                punkte = self.cleaned_data[key]
                element_id = int(index)
                override = self.overrides.get(element_id)
                if override:
                    override.delete()
                if punkte is not None:
                    override = ZeugnisKompetenzNote.objects.create(
                        schüler=schüler,
                        periode=periode,
                        element_id=element_id,
                        note=punkte,
                    ).save()
            elif kind == "note":
                note = self.cleaned_data[key]

                if self.prädikatnote:
                    self.prädikatnote.delete()

                if note:
                    ZeugnisPrädikatNote.objects.create(
                        schüler=schüler,
                        periode=periode,
                        abschnitt=self.abschnitt,
                        note=note,
                    ).save()
            elif kind == "kommentar":
                text = self.cleaned_data[key]
                if self.kommentar:
                    self.kommentar.delete()

                if text.strip() != "":
                    ZeugnisFachKommentar.objects.create(
                        schüler=schüler,
                        periode=periode,
                        abschnitt=self.abschnitt,
                        kommentar=text,
                    ).save()


class ZeugnisSozialAbschnittForm(Form):
    error_css_class = "is-danger"

    def __init__(self, request, abschnitt, zeugnis, *args, **kwargs):
        super(ZeugnisSozialAbschnittForm, self).__init__(
            request, *args, **kwargs)
        self.zeugnis = zeugnis
        self.abschnitt = abschnitt
        self.bewertungen = dict()
        for kompetenz in abschnitt.sozialkompetenzen:
            note = kompetenz.vlt_sozialnote[0] if kompetenz.vlt_sozialnote else None
            self.bewertungen[kompetenz.id] = note
            self.fields[kompetenz.id] = ChoiceField(
                choices=((None, ''),) + ZEICHEN,
                initial=note.note if note else None,
                label=kompetenz,
                required=False,
                widget=forms.RadioSelect(),
            )

    def save(self):
        for key in self.changed_data:
            note = self.cleaned_data[key]
            kompetenz_id = int(key)
            bewertung = self.bewertungen.get(kompetenz_id)
            if bewertung:
                bewertung.delete()
            if note:
                ZeugnisSozialNote.objects.create(
                    schüler=self.zeugnis.schüler,
                    periode=self.zeugnis.periode,
                    sozialkompetenz_id=kompetenz_id,
                    note=note,
                ).save()
