import nested_admin
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from grappelli.forms import GrappelliSortableHiddenMixin
from django.forms.widgets import TextInput
from django.forms import CharField, ModelForm

from .models import (AbschlussTest, BausteinFarbe, Baustein, BausteinAbschnitt, BausteinArbeit, Fach,
                     Klasse, Kompetenz, LehrerRolle, Leistung, Schüler, SozialKompetenz,
                     SozialKompetenzAbschnitt, Teacher, Test, VersetzungsBedingung, ZeugnisAbschnitt,
                     ZeugnisElement, ZeugnisVorlage)


class SchülerInline(admin.TabularInline):
    model = Schüler
    fields = ('name', 'vorname')
    extra = 0

class LehrerRolleInline(admin.TabularInline):
    model = LehrerRolle
    fields = ('fach', 'lehrer')
    extra = 0

class KlasseAdmin(admin.ModelAdmin):
    inlines = [
        SchülerInline,
        LehrerRolleInline
    ]

class SchülerAdmin(admin.ModelAdmin):
    fields = ('vorname', 'name', 'klasse', 'noten')
    list_display = ('__str__', 'klasse', 'noten')
    readonly_fields = ('noten', )


class KompetenzInline(admin.TabularInline):
    model = Kompetenz


class FachAdmin(admin.ModelAdmin):
    inlines = [
        KompetenzInline,
    ]


class ElementInline(GrappelliSortableHiddenMixin, nested_admin.NestedTabularInline):
    model = ZeugnisElement
    sortable_field_name = "ord_inner"
    extra = 0


class ZeugnisAbschnittInline(GrappelliSortableHiddenMixin, nested_admin.NestedTabularInline):
    model = ZeugnisAbschnitt
    sortable_field_name = "ord_outer"
    inlines = [ElementInline]
    extra = 0


class SozialKompetenzInline(GrappelliSortableHiddenMixin, nested_admin.NestedTabularInline):
    model = SozialKompetenz
    sortable_field_name = "ord_inner"
    extra = 0


class SozialKompetenzAbschnittInline(GrappelliSortableHiddenMixin, nested_admin.NestedTabularInline):
    model = SozialKompetenzAbschnitt
    sortable_field_name = "ord_outer"
    inlines = [SozialKompetenzInline]
    extra = 0


class VorlageAdmin(nested_admin.NestedModelAdmin):
    inlines = [ZeugnisAbschnittInline, SozialKompetenzAbschnittInline]


class LeistungInline(GrappelliSortableHiddenMixin, nested_admin.NestedTabularInline):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "kompetenz":
            kwargs["queryset"] = Kompetenz.objects.all().prefetch_related('fach')
        return super().formfield_for_foreignkey(db_field, request, **kwargs)
    model = Leistung
    sortable_field_name = "ord_inner"
    extra = 0


class TestAdmin(admin.ModelAdmin):
    model = Test
    inlines = [
        LeistungInline
    ]

class AbschlussTestAdmin(admin.ModelAdmin):
    model = AbschlussTest
    list_display = ('name', 'kompetenz')


class BausteinAbschnittInline(GrappelliSortableHiddenMixin, nested_admin.NestedStackedInline):
    model = BausteinAbschnitt
    sortable_field_name = "ord_outer"
    inlines = [LeistungInline]
    extra = 0


class BausteinAdmin(nested_admin.NestedModelAdmin):
    inlines = [BausteinAbschnittInline]
    list_display = ('name', 'kürzel', 'farbe', 'jahrgang', 'hauptkompetenz')
    list_editable = ('farbe',)
    fields = ('name', 'kürzel', 'farbe', 'jahrgang', 'hauptkompetenz')


class BausteinFarbeAdmin(admin.ModelAdmin):
    fields = ('name', 'farbe', 'priorität')
    list_display = ('name', 'farbe', 'priorität')
    list_editable = ('farbe', 'priorität')


class VersetzungsBedingungAdmin(admin.ModelAdmin):
    fields = ('jahr', 'text')
    list_display = ('jahr', 'text')
    list_editable = ('text',)


admin.site.register(Teacher, UserAdmin)
admin.site.register(Schüler, SchülerAdmin)
admin.site.register(Klasse, KlasseAdmin)
admin.site.register(Fach, FachAdmin)
admin.site.register(BausteinArbeit)
admin.site.register(Test, TestAdmin)
admin.site.register(AbschlussTest, AbschlussTestAdmin)
admin.site.register(ZeugnisVorlage, VorlageAdmin)
admin.site.register(Baustein, BausteinAdmin)
admin.site.register(BausteinFarbe, BausteinFarbeAdmin)
admin.site.register(VersetzungsBedingung, VersetzungsBedingungAdmin)
