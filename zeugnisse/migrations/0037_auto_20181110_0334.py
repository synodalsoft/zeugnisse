# Generated by Django 2.1.2 on 2018-11-10 03:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('zeugnisse', '0036_remove_zeugnis_beschluss'),
    ]

    operations = [
        migrations.AddField(
            model_name='zeugnis',
            name='förderziele',
            field=models.TextField(blank=True, default='', max_length=1000),
        ),
        migrations.AddField(
            model_name='zeugnis',
            name='klassenleiterin',
            field=models.CharField(blank=True, default='', max_length=100),
        ),
    ]
