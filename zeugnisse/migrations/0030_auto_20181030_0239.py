# Generated by Django 2.1.2 on 2018-10-30 02:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('zeugnisse', '0029_zeugnis_sozialverhalten_kommentar'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='zeugnis',
            name='zu_besprechen',
        ),
        migrations.AddField(
            model_name='zeugnis',
            name='status',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Unfertig'), (2, 'Zu besprechen'), (3, 'Fertig')], default=1),
        ),
    ]
