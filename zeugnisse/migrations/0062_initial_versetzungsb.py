from django.db import migrations

def add_versetzungsbedingungen(apps, schema_editor):
    VersetzungsBedingung = apps.get_model('zeugnisse', 'VersetzungsBedingung')
    for jahr in range(1, 7):
        bedingung = VersetzungsBedingung(jahr=jahr, text="")
        bedingung.save()

class Migration(migrations.Migration):

    dependencies = [
        ('zeugnisse', '0061_auto_20210111_1136'),
    ]

    operations = [
        migrations.RunPython(add_versetzungsbedingungen),
    ]