# Generated by Django 2.1.1 on 2018-09-08 07:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('zeugnisse', '0009_auto_20180908_0738'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bausteinkommentar',
            old_name='text',
            new_name='kommentar',
        ),
    ]
