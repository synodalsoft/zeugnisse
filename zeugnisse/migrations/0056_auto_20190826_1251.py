# Generated by Django 2.2.2 on 2019-08-26 12:51

from django.db import migrations, models
import zeugnisse.models


class Migration(migrations.Migration):

    dependencies = [
        ('zeugnisse', '0055_auto_20190605_1343'),
    ]

    operations = [
        migrations.AlterField(
            model_name='baustein',
            name='jahrgang',
            field=models.PositiveSmallIntegerField(choices=[(1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5'), (6, '6'), (7, 'Alumni')]),
        ),
        migrations.AlterField(
            model_name='bewertung',
            name='periode',
            field=models.PositiveSmallIntegerField(choices=[(1, '1'), (2, '2')], default=zeugnisse.models.aktuelle_periode),
        ),
        migrations.AlterField(
            model_name='klasse',
            name='jahrgang',
            field=models.PositiveSmallIntegerField(choices=[(1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5'), (6, '6'), (7, 'Alumni')], default=2),
        ),
        migrations.AlterField(
            model_name='test',
            name='periode',
            field=models.PositiveSmallIntegerField(choices=[(1, '1'), (2, '2')], default=zeugnisse.models.aktuelle_periode),
        ),
        migrations.AlterField(
            model_name='zeugnis',
            name='periode',
            field=models.PositiveSmallIntegerField(choices=[(1, '1'), (2, '2')]),
        ),
        migrations.AlterField(
            model_name='zeugnisfachkommentar',
            name='periode',
            field=models.PositiveSmallIntegerField(choices=[(1, '1'), (2, '2')]),
        ),
        migrations.AlterField(
            model_name='zeugniskompetenznote',
            name='periode',
            field=models.PositiveSmallIntegerField(choices=[(1, '1'), (2, '2')]),
        ),
        migrations.AlterField(
            model_name='zeugnisprädikatnote',
            name='periode',
            field=models.PositiveSmallIntegerField(choices=[(1, '1'), (2, '2')]),
        ),
        migrations.AlterField(
            model_name='zeugnissozialnote',
            name='periode',
            field=models.PositiveSmallIntegerField(choices=[(1, '1'), (2, '2')]),
        ),
    ]
