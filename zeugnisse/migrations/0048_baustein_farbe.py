# Generated by Django 2.1.7 on 2019-02-22 10:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('zeugnisse', '0047_bausteinfarbe_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='baustein',
            name='farbe',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='zeugnisse.BausteinFarbe'),
        ),
    ]
