from statistics import mean
from collections import defaultdict
from decimal import Decimal

from zeugnisse.models import (
    BausteinArbeitBewertung, LeistungsBewertung, AbschlussTestBewertung, TestKommentar, ZeugnisVorlage)


def anteil(punkte, max_punkte):
    return Decimal(0) if max_punkte == 0 else punkte / max_punkte


def prozentsatz(punkte, max_punkte):
    return 100 * anteil(punkte, max_punkte)


def gesamtpunkte_pro_kompetenz(schüler, periode):
    gesamtpunkte = defaultdict(lambda: [Decimal(0), Decimal(0)])

    leistungsbewertungen = LeistungsBewertung.objects.filter(
        schüler=schüler, periode=periode).select_related(
        'leistung', 'leistung__kompetenz')
    for lb in leistungsbewertungen:
        gesamtpunkte[lb.leistung.kompetenz][0] += lb.punkte
        gesamtpunkte[lb.leistung.kompetenz][1] += lb.leistung.max_punkte

    bausteinbewertungen = BausteinArbeitBewertung.objects.filter(
        schüler=schüler, periode=periode).select_related(
        'arbeit', 'baustein__hauptkompetenz')
    for bb in bausteinbewertungen:
        gesamtpunkte[bb.baustein.hauptkompetenz][0] += bb.punkte
        gesamtpunkte[bb.baustein.hauptkompetenz][1] += bb.arbeit.max_punkte

    return gesamtpunkte


def testkommentare_pro_abschnitt(schüler, periode):
    vorlage = ZeugnisVorlage.objects.get(stufe=schüler.klasse.jahrgang)
    abschnitte = dict(
        vorlage.zeugnisabschnitt_set.all().values_list('fach', 'id'))
    kommentare = TestKommentar.objects.filter(
        schüler=schüler, test__periode=periode).select_related(
        'test', 'test__fach')

    k_dict = defaultdict(lambda: [])
    for k in kommentare:
        k_dict[abschnitte[k.test.fach_id]] += [k]
    return k_dict


def alle_abschlussbewertungen(schüler):
    res = dict()
    for bewertung in AbschlussTestBewertung.objects.filter(schüler=schüler):
        k_id = bewertung.test.kompetenz.id
        res.setdefault(k_id, [])
        res[k_id] += [bewertung]
    return res


def berechne_note(element, periode, gesamtpunkte, abschlussbewertungen):
    kompetenz = element.kompetenz

    gewertete_punkte = []
    if kompetenz in gesamtpunkte:
        punkte_normal = element.max_punkte * \
            anteil(gesamtpunkte[kompetenz][0],
                   gesamtpunkte[kompetenz][1])
        gewertete_punkte += [punkte_normal]

    if kompetenz.mit_abschlusstests and periode == 2 and kompetenz.id in abschlussbewertungen:
        bewertungen = abschlussbewertungen[kompetenz.id]

        habe, soll = Decimal(0), Decimal(0)
        for bewertung in bewertungen:
            habe += bewertung.punkte
            soll += bewertung.test.max_punkte
        punkte_abschluss = element.max_punkte * anteil(habe, soll)
        gewertete_punkte += [punkte_abschluss]

    if gewertete_punkte != []:
        return mean(gewertete_punkte)
    else:
        return None


def pre_prozent(kompetenz, gesamtpunkte):
    if kompetenz in gesamtpunkte:
        return prozentsatz(gesamtpunkte[kompetenz][0], gesamtpunkte[kompetenz][1])
    else:
        return None


def prüfung_prozent(kompetenz, abschlussbewertungen):
    if kompetenz.id in abschlussbewertungen:
        bs = abschlussbewertungen[kompetenz.id]

        habe, soll = Decimal(0), Decimal(0)
        for b in bs:
            habe += b.punkte
            soll += b.test.max_punkte
        return prozentsatz(habe, soll)
    else:
        return None
