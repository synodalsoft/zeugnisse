from django.apps import AppConfig


class ZeugnisseConfig(AppConfig):
    name = 'zeugnisse'
