from collections import OrderedDict

from django.contrib.auth.decorators import login_required
from django.db.models import Count, F, Prefetch, Sum, Value, Q
from django.shortcuts import render, reverse

from zeugnisse.models import (Bewertung, Klasse, Kompetenz, Leistung,
                              LeistungsBewertung, Schüler)


@login_required
def kompetenz_l(request, klasse_id, kompetenz_id):
    klasse = Klasse.objects.get(id=klasse_id)
    kompetenz = Kompetenz.objects.get(id=kompetenz_id)
    perioden = OrderedDict([
        (
            periode,
            Leistung.objects.filter(kompetenz=kompetenz)
            .prefetch_related(Prefetch('leistungsbewertung_set',
                                       queryset=LeistungsBewertung.objects.filter(
                                           schüler__klasse=klasse, periode=periode).order_by('schüler__name'),
                                       to_attr='bewertungen'
                                       ))
            .annotate(prozentsatz=Value(100) * Sum('leistungsbewertung__punkte', filter=Q(leistungsbewertung__periode=periode)) /
                      Sum('leistungsbewertung__leistung__max_punkte', filter=Q(leistungsbewertung__periode=periode))
                      )
        )
        for periode in [1, 2]
    ])

    context = {
        'title': '{} nach Leistung'.format(kompetenz),
        'breadcrumbs': [
            ('Klasse {}'.format(klasse), reverse('klasse', args=[klasse.id])),
        ],
        'klasse': klasse,
        'kompetenz': kompetenz,
        'perioden': perioden,
    }
    return render(request, 'zeugnisse/kompetenz_l.html', context)


@login_required
def kompetenz_s(request, klasse_id, kompetenz_id):
    klasse = Klasse.objects.get(id=klasse_id)
    kompetenz = Kompetenz.objects.get(id=kompetenz_id)
    perioden = OrderedDict([
        (
            periode,
            klasse.schüler_set.filter(
                bewertung__leistungsbewertung__leistung__kompetenz=kompetenz)
            .order_by('name')
            .distinct()
            .prefetch_related(Prefetch('bewertung_set',
                                       queryset=Bewertung.objects.filter(
                                           leistungsbewertung__leistung__kompetenz=kompetenz, periode=periode),
                                       to_attr='bewertungen'
                                       ))
            .annotate(prozentsatz=Value(100) * Sum('bewertung__leistungsbewertung__punkte', filter=Q(bewertung__periode=periode)) /
                      Sum('bewertung__leistungsbewertung__leistung__max_punkte', filter=Q(bewertung__periode=periode))
                      )
        )
        for periode in [1, 2]
    ])

    context = {
        'title': '{} nach Schüler'.format(kompetenz),
        'breadcrumbs': [
            ('Klasse {}'.format(klasse), reverse('klasse', args=[klasse.id])),
        ],
        'klasse': klasse,
        'kompetenz': kompetenz,
        'perioden': perioden,
    }
    return render(request, 'zeugnisse/kompetenz_s.html', context)
