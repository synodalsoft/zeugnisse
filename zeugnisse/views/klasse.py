from collections import OrderedDict

from django.contrib.auth.decorators import login_required
from django.db.models import Count, Q
from django.shortcuts import render
from django.views.generic.edit import UpdateView

from zeugnisse.models import (Baustein, BausteinArbeit,
                              BausteinArbeitBewertung, BausteinKommentar, Fach,
                              Klasse, Kompetenz, LeistungsBewertung)


@login_required
def klasse(request, klasse_id):
    klasse = Klasse.objects.get(pk=klasse_id)
    testfächer = Fach.objects.filter(
        advanced=True,
        zeugnisabschnitt__vorlage__stufe=klasse.stufe(),
    ).annotate(num_tests=Count("test", filter=Q(test__klasse=klasse)))
    kompetenzen = Kompetenz.objects.filter(
        zeugniselement__abschnitt__vorlage__stufe=klasse.stufe()
    ).order_by("fach").select_related("fach")

    bausteine = Baustein.objects.filter(jahrgang=klasse.jahrgang).order_by("farbe").annotate(
        gesamt=Count("bausteinabschnitt__leistung"))

    schüler = OrderedDict([
        (s, OrderedDict([(b, {
            "bewertungen": 0,
            "bausteinarbeiten": 0,
            "kommentar": False,
        }) for b in bausteine
        ]))
        for s in klasse.schüler_set.all()
    ])

    for kommentar in BausteinKommentar.objects.filter(
            schüler__klasse=klasse).select_related(
            'schüler', 'baustein'):
        if kommentar.kommentar.strip() != "":
            schüler[kommentar.schüler][kommentar.baustein]["kommentar"] = True

    for bewertung in LeistungsBewertung.objects.filter(
        schüler__klasse=klasse,
        leistung__block__bausteinabschnitt__baustein__jahrgang=klasse.jahrgang,
    ).select_related(
        'schüler',
            'leistung__block__bausteinabschnitt__baustein'):
        schüler[bewertung.schüler][bewertung.leistung.block.bausteinabschnitt.baustein]["bewertungen"] += 1

    for bewertung in BausteinArbeitBewertung.objects.filter(
        schüler__klasse=klasse,
        baustein__jahrgang=klasse.jahrgang).select_related(
        'schüler',
            'baustein'):
        schüler[bewertung.schüler][bewertung.baustein]["bausteinarbeiten"] += 1

    num_bausteinarbeiten = BausteinArbeit.objects.all().count()
    for bs in schüler.values():
        for info in bs.values():
            if info["kommentar"] and (
                    info["bausteinarbeiten"] == num_bausteinarbeiten):
                info["farbe"] = "success"
            elif info["bewertungen"] > 0 or info["kommentar"] or info["bausteinarbeiten"]:
                info["farbe"] = "warning"
            else:
                info["farbe"] = "white"

    context = {
        "title": "Klasse {}".format(klasse),
        "klasse": klasse,
        "testfächer": testfächer,
        "schüler": schüler,
        "kompetenzen": kompetenzen,
    }
    return render(request, 'zeugnisse/klasse.html', context)


class KlasseBearbeiten(UpdateView):
    model = Klasse
    fields = [
        'klassenleiterinnen',
        'ka_religionslehrerin',
        'ev_religionslehrerin',
        'sportlehrerin',
        'geographielehrerin',
        'geschichtslehrerin',
        'techniklehrerin']
