from collections import OrderedDict

from django.contrib.auth.decorators import login_required
from django.db.models import Count, Value, Prefetch
from django.shortcuts import redirect, render, reverse

from zeugnisse.models import Fach, Klasse, Test, AbschlussTest, Kompetenz, aktuelle_periode


@login_required
def tests(request, klasse_id, fach_id):
    fach = Fach.objects.get(id=fach_id)
    klasse = Klasse.objects.filter(
        pk=klasse_id).prefetch_related('schüler_set').get()
    perioden = OrderedDict([(p, Test.objects.filter(
        klasse=klasse, fach=fach, periode=p).annotate(
            progress=Value(100) * (Value(0.01) + Count('leistung__leistungsbewertung', distinct=True)) /
            (Value(0.01) + (Count('leistung', distinct=True) * klasse.schüler_set.count())),
    )) for p in [2, 1]])

    abschlusskompetenzen = Kompetenz.objects.filter(
        zeugniselement__abschnitt__vorlage__stufe=klasse.stufe(),
        fach=fach,
        mit_abschlusstests=True).prefetch_related(
            Prefetch('abschlusstest_set',
                     queryset=AbschlussTest.objects.all(),
                     to_attr='tests'
                     )
            ) if klasse.jahrgang == 6 else None

    context = {
        "title": "{}".format(fach),
        'breadcrumbs': [
            ('Klasse {}'.format(klasse), reverse('klasse', args=[klasse_id])),
        ],
        "klasse": klasse,
        "perioden": perioden,
        "fach": fach,
        "abschlusskompetenzen": abschlusskompetenzen,
    }
    return render(request, 'zeugnisse/tests.html', context)


@login_required
def delete_test(request, test_id):
    test = Test.objects.get(id=test_id)
    if request.POST:
        test.delete()
        return redirect('tests', test.klasse.id, test.fach_id)
    return render(request, 'zeugnisse/delete_test.html', {
        'test': test,
    })