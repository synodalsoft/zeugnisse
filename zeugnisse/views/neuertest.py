from django.contrib.auth.decorators import login_required
from django.forms import DecimalField, ModelForm, NumberInput, ValidationError
from django.shortcuts import redirect, render, reverse

from zeugnisse.models import Fach, Klasse, Kompetenz, Leistung, Test, AbschlussTest


class NewTestForm(ModelForm):
    class Meta:
        model = Test
        fields = ("name", "periode")

    def __init__(self, klasse, fach, periode, *args, **kwargs):
        super(NewTestForm, self).__init__(*args, **kwargs)
        self.fach = fach
        self.klasse = klasse
        self.fields["periode"].initial = periode
        for k in Kompetenz.objects.filter(
                zeugniselement__abschnitt__vorlage__stufe=klasse.stufe(),
                fach=fach):
            self.fields["leistung/{}".format(k.id)] = DecimalField(
                min_value=0,
                max_value=100,
                initial=0,
                label=str(k.beschreibung),
                label_suffix="",
                required=False,
                localize=True,
                widget=NumberInput(
                    attrs={
                        'class': 'input is-radiusless is-shadowless is-medium',
                        'type': 'number',
                        'min': 0,
                        'max': 100,
                        'step': 0.1,
                    })
            )

    def clean(self):
        cleaned_data = super().clean()
        has_data = False
        for key, value in cleaned_data.items():
            if "leistung/" in key and value > 0:
                has_data = True

        if not has_data:
            raise ValidationError(
                "Mindestens eine Kompetenz muss mit mehr als 0 Punkten gewertet werden.",
                code="no_kompetenz")

    def save(self, commit=True):
        m = super(NewTestForm, self).save(commit=False)
        if commit:
            m.fach = self.fach
            m.klasse = self.klasse
            m.save()
            for key, data in self.cleaned_data.items():
                if "leistung/" in key:
                    leistung = Leistung(
                        kompetenz=Kompetenz.objects.get(
                            id=int(key.split("/")[1])),
                        max_punkte=data,
                        block=m)
                    if data > 0:
                        leistung.save()
        return m


@login_required
def neuertest(request, klasse_id, fach_id, periode):
    lehrer = request.user
    klasse = Klasse.objects.get(id=klasse_id)
    fach = Fach.objects.get(id=fach_id)

    testform = NewTestForm(klasse, fach, periode, request.POST or None)

    if testform.is_valid():
        testform.save()
        return redirect('tests', klasse_id, fach_id)

    return render(request, 'zeugnisse/neuertest.html', {
        "title": "Neuer Test",
        'breadcrumbs': [
            ('Klasse {}'.format(klasse), reverse('klasse', args=[klasse_id])),
            ('{}'.format(fach), reverse('tests', args=[klasse_id, fach_id])),
        ],
        "klasse": klasse,
        "fach": fach,
        "testform": testform,
    })