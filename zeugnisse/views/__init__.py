from .index import index
from .klasse import klasse, KlasseBearbeiten
from .baustein import baustein
from .tests import tests, delete_test
from .test import test
from .neuertest import neuertest
from .abschlusstest import abschlusstest
from .kompetenz import kompetenz_s, kompetenz_l
from .zeugnis import zeugnis, zeugnisse_print, zeugnisse_export, kommentare_export
from .zeugnisse import zeugnisse
