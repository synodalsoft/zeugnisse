from django.contrib.auth.decorators import login_required
from django.db.models import Prefetch
from django.shortcuts import redirect, render, reverse

from zeugnisse.models import LeistungsBewertung, Test, TestKommentar
from zeugnisse.forms import TestFieldForm, TestKommentarForm


@login_required
def test(request, test_id):
    test = Test.objects.get(id=test_id)
    klasse = test.klasse
    leistungen = test.leistung_set.all().prefetch_related(
        Prefetch(
            'leistungsbewertung_set',
            to_attr="bewertungen",
        )
    )
    initials = {schüler: dict() for schüler in klasse.schüler_set.all()}
    for leistung in leistungen:
        for bewertung in leistung.bewertungen:
            initials[bewertung.schüler][bewertung.leistung] = bewertung.punkte

    kommentare = {
            kommentar.schüler_id: kommentar
        for kommentar in test.testkommentar_set.all()
    }

    table = [
        (
            schüler,
            [
                (leistung, TestFieldForm(
                    request.POST or None,
                    label = str(leistung.kompetenz.beschreibung),
                    max_punkte = leistung.max_punkte,
                    prefix="{}:{}".format(schüler.id, leistung.id),
                    initial={"punkte": str(initials[schüler].get(leistung))},
                ))
                for leistung in leistungen
            ],
            TestKommentarForm(
                request.POST or None,
                instance=kommentare.get(schüler.id),
                prefix=str(schüler.id),
            )
        )
        for schüler in klasse.schüler_set.all()
    ]

    if all([all([form.is_valid() for leistung, form in li]) and k.is_valid() for s, li, k in table]):
        for schüler, liste, kommentarform in table:
            for leistung, form in liste:
                punkte = form.cleaned_data["punkte"]
                try:
                    bewertung = LeistungsBewertung.objects.get(
                        schüler=schüler, leistung=leistung)
                    if punkte is not None:
                        bewertung.punkte = punkte
                        bewertung.lehrer = request.user
                        bewertung.periode = test.periode
                        bewertung.save()
                    else:
                        bewertung.delete()
                except LeistungsBewertung.DoesNotExist:
                    if punkte is not None:
                        LeistungsBewertung(
                            schüler=schüler,
                            leistung=leistung,
                            punkte=punkte,
                            lehrer=request.user,
                            periode=test.periode).save()
            print(kommentarform.cleaned_data)
            instance = kommentarform.save(commit=False)
            print(instance)
            if instance.kommentar is not None and instance.kommentar.strip() != "":
                instance.schüler=schüler
                instance.test=test
                instance.save()
            elif instance.pk is not None:
                instance.delete()
        return redirect('tests', klasse.id, test.fach_id)

    context = {
        "title": "{}".format(test),
        'breadcrumbs': [
            ('Klasse {}'.format(klasse), reverse('klasse', args=[klasse.id])),
            ('{}'.format(test.fach), reverse('tests', args=[klasse.id, test.fach_id])),
        ],
        "test": test,
        "leistungen": leistungen,
        "klasse": klasse,
        "table": table,
    }
    return render(request, 'zeugnisse/test.html', context)
