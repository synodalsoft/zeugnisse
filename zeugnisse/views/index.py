from django.contrib.auth.decorators import login_required
from django.db.models import Count
from django.shortcuts import render

from zeugnisse.models import Klasse


@login_required
def index(request):
    klassen = Klasse.objects.filter(jahrgang__lte = 6).order_by(
        'jahrgang', 'name').annotate(schülerzahl=Count('schüler'))
    context = {
        "klassen": klassen,
    }
    return render(request, 'zeugnisse/index.html', context)
