from django.contrib.auth.decorators import login_required
from django.db.models import Prefetch
from django.forms import DecimalField, Form, NumberInput
from django.shortcuts import redirect, render, reverse

from zeugnisse.models import AbschlussTest, AbschlussTestBewertung, Klasse
from zeugnisse.forms import TestFieldForm


@login_required
def abschlusstest(request, klasse_id, test_id):
    klasse = Klasse.objects.get(id=klasse_id)
    test = AbschlussTest.objects.get(id=test_id)
    bewertungen = AbschlussTestBewertung.objects.filter(test=test)

    initials = { bewertung.schüler : bewertung.punkte for bewertung in bewertungen }

    table = [
        (
            schüler,
            TestFieldForm(
                    request.POST or None,
                    label = test.name,
                    max_punkte = test.max_punkte,
                    prefix="{}".format(schüler.id),
                    initial={"punkte": str(initials.get(schüler))},
                )
        )
        for schüler in klasse.schüler_set.all()
    ]

    if all([form.is_valid() for s, form in table]):
        for schüler, form in table:
            punkte = form.cleaned_data["punkte"]
            try:
                bewertung = AbschlussTestBewertung.objects.get(
                    schüler=schüler, test=test)
                if punkte is not None:
                    bewertung.punkte = punkte
                    bewertung.lehrer = request.user
                    bewertung.save()
                else:
                    bewertung.delete()
            except AbschlussTestBewertung.DoesNotExist:
                if punkte is not None:
                    AbschlussTestBewertung(
                        schüler=schüler,
                        test=test,
                        punkte=punkte,
                        lehrer=request.user).save()
        return redirect('tests', klasse.id, test.kompetenz.fach_id)

    context = {
        "title": "Abschlusstest {}".format(test),
        'breadcrumbs': [
            ('Klasse {}'.format(klasse), reverse('klasse', args=[klasse_id])),
            ('{}'.format(test.kompetenz.fach), reverse('tests', args=[klasse_id, test.kompetenz.fach_id])),
        ],
        "test": test,
        "klasse": klasse,
        "table": table,
    }
    return render(request, 'zeugnisse/abschlusstest.html', context)
