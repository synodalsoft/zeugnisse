from collections import OrderedDict

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, reverse

from zeugnisse.models import Klasse, Schüler, Zeugnis


@login_required
def zeugnisse(request, klasse_id):
    klasse = Klasse.objects.get(pk=klasse_id)

    table = OrderedDict([
        (schüler, ['Unfertig' for p in [1, 2]])
        for schüler in klasse.schüler_set.all()])

    for zeugnis in Zeugnis.objects.filter(schüler__klasse=klasse):
        table[zeugnis.schüler][zeugnis.periode -
                               1] = zeugnis.get_status_display()

    context = {
        "title": "Zeugnisse",
        'breadcrumbs': [
            ('Klasse {}'.format(klasse), reverse('klasse', args=[klasse.id])),
        ],
        "klasse": klasse,
        "table": table,
    }
    return render(request, 'zeugnisse/zeugnisse.html', context)
