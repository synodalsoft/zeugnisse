import io
import csv
from collections import OrderedDict, defaultdict
from decimal import Decimal
from statistics import mean
# import logging; logger = logging.getLogger('zeugnisse')
import logging; logger = logging.getLogger('django')

import weasyprint
from weasyprint.text.fonts import FontConfiguration

from django.contrib.auth.decorators import login_required
from django.db.models import Prefetch
from django.http import FileResponse
from django.shortcuts import redirect, render, reverse
from django.template.loader import render_to_string
from django.utils import timezone

from zeugnisse.forms import (ZeugnisAbschnittForm, ZeugnisForm,
                             ZeugnisSozialAbschnittForm)
from zeugnisse.models import (BausteinArbeitBewertung, Klasse, LehrerRolle, Leistung, LeistungsBewertung,
                              Schüler, SozialKompetenz, Teacher, Test, TestKommentar, VersetzungsBedingung,
                              Zeugnis, ZeugnisAbschnitt, ZeugnisElement, ZeugnisFachKommentar,
                              ZeugnisKompetenzNote, ZeugnisPrädikatNote,
                              ZeugnisSozialNote, AbschlussTestBewertung, Fach,
                              PRÄDIKATE, ZEICHEN)

from zeugnisse.logic import (anteil, gesamtpunkte_pro_kompetenz, berechne_note,
                             prozentsatz, alle_abschlussbewertungen, pre_prozent,
                             prüfung_prozent, testkommentare_pro_abschnitt)

CSV_OPTIONS = dict(delimiter=';')

def csvnumber(s):
    return s.replace(".", ",")

def zeugnis_html(request, zeugnis, bis_periode):
    schüler = zeugnis.schüler
    klasse = schüler.klasse
    vorlage = zeugnis.vorlage()
    perioden = range(1, bis_periode + 1)

    kommentare = dict(ZeugnisFachKommentar.objects.filter(
        schüler=schüler, periode=bis_periode).values_list('abschnitt', 'kommentar'))

    prädikatnoten = dict()
    for periode, abschnitt, note in ZeugnisPrädikatNote.objects.filter(schüler=schüler).values_list('periode', 'abschnitt', 'note'):
        prädikatnoten.setdefault(periode, {}).update({abschnitt: note})

    overrides = dict()
    for periode, element, note in ZeugnisKompetenzNote.objects.filter(schüler=schüler).values_list('periode', 'element', 'note'):
        overrides.setdefault(periode, {}).update({element: note})

    abschlussbewertungen = alle_abschlussbewertungen(schüler)

    gesamtpunkte = {periode: gesamtpunkte_pro_kompetenz(
        schüler, periode) for periode in perioden}

    blöcke = []
    for abschnitt in vorlage.zeugnisabschnitt_set.all():
        fachlehrer = []
        if abschnitt.fach.name == "Religion":
            if zeugnis.katholische_religion:
                if klasse.ka_religionslehrerin != "":
                    fachlehrer += [("Katholische Religion", klasse.ka_religionslehrerin)]
            else:
                if klasse.ev_religionslehrerin != "":
                    fachlehrer += [("Evangelische Religion", klasse.ev_religionslehrerin)]
        elif abschnitt.fach.name == "Sport":
            if klasse.sportlehrerin != "":
                fachlehrer += [("Sport", klasse.sportlehrerin)]
        elif abschnitt.fach.name == "Wissenschaften":
            if klasse.geographielehrerin != "":
                fachlehrer += [("Geographie", klasse.geographielehrerin)]
            if klasse.geschichtslehrerin != "":
                fachlehrer += [("Geschichte", klasse.geschichtslehrerin)]
            if klasse.techniklehrerin != "":
                fachlehrer += [("Technik", klasse.techniklehrerin)]

        block = {
            "fach": abschnitt.fach.name,
            "fixtext": abschnitt.fixtext,
            "fachlehrer": fachlehrer,
            "kommentar": kommentare.get(abschnitt.id, ""),
        }
        if abschnitt.fach.advanced:
            block["advanced"] = True
            block["prüfungen"] = False
            block["elemente"] = []
            block["verlauf"] = {p: {
                "punkte": Decimal(0),
                "max_punkte": Decimal(0),
            } for p in [1, 2]}

            for element in abschnitt.zeugniselement_set.select_related("kompetenz"):
                kompetenz = element.kompetenz
                max_punkte = element.max_punkte
                zeile = {
                    "kompetenz": kompetenz.beschreibung,
                    "max_punkte": max_punkte,
                    "verlauf": OrderedDict([
                        (p, None)
                        for p in [1, 2]
                    ])
                }
                for periode in perioden:
                    if periode in overrides and element.id in overrides[periode]:
                        punkte = overrides[periode][element.id]
                    else:
                        punkte = berechne_note(
                            element, periode, gesamtpunkte[periode], abschlussbewertungen)

                    if punkte is None:
                        continue

                    zeile["verlauf"][periode] = {
                        "punkte": punkte,
                        "prozentsatz": prozentsatz(punkte, max_punkte)
                    }

                    block["verlauf"][periode]["punkte"] += punkte
                    block["verlauf"][periode]["max_punkte"] += max_punkte

                if bis_periode == 2 and zeile["verlauf"][2] is not None:
                    if kompetenz.id in abschlussbewertungen:
                        block["prüfungen"] = True
                        zeile["verlauf"][2]["prüfung_prozent"] = prüfung_prozent(
                            kompetenz, abschlussbewertungen)
                        zeile["verlauf"][2]["pre_prozent"] = pre_prozent(
                            kompetenz, gesamtpunkte[periode])
                    else:
                        zeile["verlauf"][2]["pre_prozent"] = zeile["verlauf"][2]["prozentsatz"]

                zeile["aktuell"] = zeile["verlauf"][bis_periode]

                alle_punkte = list(map(lambda v: v["punkte"], filter(
                    lambda v: v is not None, zeile["verlauf"].values())))
                zeile["durchschnitt"] = mean(alle_punkte) if len(
                    alle_punkte) > 0 else None
                block["elemente"] += [zeile]

            for p in perioden:
                if block["verlauf"][p]["max_punkte"] > 0:
                    block["verlauf"][p]["prozentsatz"] = prozentsatz(
                        block["verlauf"][p]["punkte"], block["verlauf"][p]["max_punkte"])

            alle_prozentsätze = list(map(lambda v: v["prozentsatz"], filter(
                lambda v: "prozentsatz" in v, block["verlauf"].values())))

            block["gesamt"] = {
                "aktuell": block["verlauf"][bis_periode],
                "durchschnitt": mean(alle_prozentsätze) if len(alle_prozentsätze) > 0 else None
            }

        else:
            block["advanced"] = False
            block["verlauf"] = OrderedDict([
                (1, ""),
                (2, ""),
            ])
            for periode in perioden:
                if periode in prädikatnoten and abschnitt.id in prädikatnoten[periode]:
                    prädikatnote = prädikatnoten[periode][abschnitt.id]
                    block["verlauf"][periode] = PRÄDIKATE[prädikatnote - 1][1]

            block["aktuell"] = block["verlauf"][bis_periode]
        blöcke += [block]

    versetzungsbedingungen = VersetzungsBedingung.objects.get(jahr=klasse.jahrgang).text

    sozialnoten = dict(ZeugnisSozialNote.objects.filter(
        schüler=schüler, periode=bis_periode).values_list("sozialkompetenz", "note"))
    sozialabschnitte = [(
        abschnitt.titel,
        [(kompetenz, sozialnoten.get(kompetenz.id))
         for kompetenz in abschnitt.sozialkompetenz_set.all()]
    ) for abschnitt in vorlage.sozialkompetenzabschnitt_set.all()]

    context = {
        "schüler": schüler,
        "periode": bis_periode,
        "zeugnis": zeugnis,
        "notenblöcke": blöcke,
        "zeichens": ZEICHEN,
        "sozialabschnitte": sozialabschnitte,
        "abschlussbewertungen": abschlussbewertungen,
        "date": timezone.now().date(),
        "versetzungsbedingungen": versetzungsbedingungen,
    }

    html = weasyprint.HTML(string=render_to_string(
        'zeugnisse/zeugnis_pdf.html', context),
        base_url=request.build_absolute_uri())
    return html

def zeugnis_print(request, schüler, bis_periode, as_attachment):
    zeugnis = Zeugnis.objects.get_or_create(
        schüler=schüler, periode=bis_periode)[0]
    html = zeugnis_html(request, zeugnis, bis_periode)
    css = weasyprint.CSS(filename='static/bulma/css/style.min.css')
    filename = ("Zeugnis-{}-periode-{}.pdf".format(schüler, bis_periode))
    buffer = io.BytesIO()
    html.write_pdf(buffer, stylesheets=[css])
    buffer.seek(0)
    return FileResponse(buffer, as_attachment=as_attachment, filename=filename)

@login_required
def zeugnisse_print(request, klasse_id, bis_periode, as_attachment):
    klasse = Klasse.objects.get(id=klasse_id)

    css = weasyprint.CSS(filename='static/bulma/css/style.min.css')
    font_config = FontConfiguration()

    docs = []
    for schüler in klasse.schüler_set.all():
        zeugnis = Zeugnis.objects.get_or_create(
            schüler=schüler, periode=bis_periode)[0]
        html = zeugnis_html(request, zeugnis, bis_periode)
        doc = html.render(stylesheets=[css], enable_hinting=False)

        if(len(doc.pages) % 2 == 1):
            emtpy_html = weasyprint.HTML(string="<html></html>")
            empty_doc = emtpy_html.render()
            doc.pages += [empty_doc.pages[0]]

        docs += [doc]
    buffer = io.BytesIO()
    # doc = weasyprint.Document(pages=[page for doc in docs for page in doc.pages], metadata=docs[0].metadata, url_fetcher=None, font_config=None)
    pages = [page for doc in docs for page in doc.pages]
    # logger.info("20240221 %s", pages)
    font_config = FontConfiguration()
    # logger.warning("20240221 %s", [p.font_config for p in pages])
    #doc = weasyprint.Document([page for doc in docs for page in doc.pages], docs[0].metadata, None, None)
    doc = weasyprint.Document(pages, docs[0].metadata, None, font_config)
    doc.write_pdf(buffer)
    buffer.seek(0)

    filename = ("Zeugnisse-{}-periode-{}.pdf".format(klasse, bis_periode))
    return FileResponse(buffer, as_attachment=as_attachment, filename=filename)


@login_required
def zeugnis(request, schüler_id, periode):
    schüler = Schüler.objects.get(id=schüler_id)
    zeugnis = Zeugnis.objects.get_or_create(
        schüler=schüler, periode=periode)[0]
    vorlage = zeugnis.vorlage()

    main_form = ZeugnisForm(request.POST or None,
                            instance=zeugnis, prefix="main_form")

    gesamtpunkte = gesamtpunkte_pro_kompetenz(schüler, periode)
    abschlussbewertungen = alle_abschlussbewertungen(schüler)

    abschnitt_forms = OrderedDict([
        (
            abschnitt,
            ZeugnisAbschnittForm(request.POST or None, abschnitt, zeugnis,
                                 gesamtpunkte, abschlussbewertungen,
                                 prefix="abschnitt{}".format(abschnitt.id)),
        )
        for abschnitt in vorlage.zeugnisabschnitt_set.all()
        .select_related('fach')
        .prefetch_related(
            Prefetch(
                'zeugniselement_set',
                to_attr='elemente',
                queryset=ZeugnisElement.objects.all().prefetch_related(
                    Prefetch(
                        'zeugniskompetenznote_set',
                        to_attr='vlt_kompetenznote',
                        queryset=ZeugnisKompetenzNote.objects.filter(
                            schüler=schüler, periode=periode)
                    ),
                ).select_related('kompetenz')
            ),
            Prefetch(
                'zeugnisfachkommentar_set',
                to_attr='vlt_fachkommentar',
                queryset=ZeugnisFachKommentar.objects.filter(
                    schüler=schüler, periode=periode)
            ),
            Prefetch(
                'zeugnisprädikatnote_set',
                to_attr='vlt_prädikatnote',
                queryset=ZeugnisPrädikatNote.objects.filter(
                    schüler=schüler, periode=periode)
            ),
            Prefetch(
                'fach__test_set',
                queryset=Test.objects.filter(
                    klasse=schüler.klasse,
                    periode=periode,
                    testkommentar__schüler=schüler,
                ).prefetch_related(
                    Prefetch(
                        'testkommentar_set',
                        queryset=TestKommentar.objects.filter(schüler=schüler),
                        to_attr='kommentare',
                    )
                ).prefetch_related(
                    Prefetch(
                        'leistung_set',
                        queryset=Leistung.objects.all()
                            .prefetch_related(
                                Prefetch(
                                    'leistungsbewertung_set',
                                    queryset=LeistungsBewertung.objects.filter(schüler=schüler),
                                    to_attr="leistungsbewertungen",
                                )
                            ),
                        to_attr='leistungen',
                    )
                ),
                to_attr='tests',
            ),
        )
    ])

    sozial_forms = [
        (
            abschnitt.titel,
            ZeugnisSozialAbschnittForm(request.POST or None, abschnitt,
                                       zeugnis, prefix="sozial{}".format(abschnitt.id)),
        )
        for abschnitt in vorlage.sozialkompetenzabschnitt_set.all().prefetch_related(
            Prefetch(
                'sozialkompetenz_set',
                to_attr='sozialkompetenzen',
                queryset=SozialKompetenz.objects.all().prefetch_related(
                    Prefetch(
                        'zeugnissozialnote_set',
                        to_attr='vlt_sozialnote',
                        queryset=ZeugnisSozialNote.objects.filter(
                            schüler=schüler, periode=periode)
                    ),
                )
            ),
        )]

    context = {
        "title": "{}, Periode {}".format(schüler, periode),
        'breadcrumbs': [
            ('Klasse {}'.format(schüler.klasse), reverse(
                'klasse', args=[schüler.klasse.id])),
            ('Zeugnisse', reverse('zeugnisse', args=[schüler.klasse_id])),
        ],
        "schüler": schüler,
        "main_form": main_form,
        "abschnitt_forms": abschnitt_forms,
        "sozial_forms": sozial_forms,
        "zeugnis": zeugnis,
    }

    if main_form.is_valid() and all(
            [form[1].is_valid() for form in list(abschnitt_forms.items()) + sozial_forms]):
        main_form.save()

        for form in list(abschnitt_forms.items()) + sozial_forms:
            form[1].save()
        if "show" in request.POST:
            return zeugnis_print(request, schüler, periode, as_attachment=False)
        if "print" in request.POST:
            return zeugnis_print(request, schüler, periode, as_attachment=True)

        return redirect('zeugnisse', schüler.klasse.id)

    return render(request, 'zeugnisse/zeugnis.html', context)


def abschluss_csv(klasse, writer):
    writer.writerow(["Name_Vor", "Deutsch", "Franz", "Rechnen", "GeErNa"])

    for schüler in klasse.schüler_set.all():
        row = [f"{schüler.name.upper()} {schüler.vorname}"]
        overrides = dict()
        for periode, element, note in ZeugnisKompetenzNote.objects.filter(schüler=schüler).values_list('periode', 'element', 'note'):
            overrides.setdefault(periode, {}).update({element: note})

        gesamtpunkte = {periode: gesamtpunkte_pro_kompetenz(
            schüler, periode) for periode in [1, 2]}
        abschlussbewertungen = alle_abschlussbewertungen(schüler)

        for (fach, abkz) in [
            ("Deutsch", "Deutsch"),
            ("Französisch", "Franz"),
            ("Mathematik", "Rechnen"),
            ("Wissenschaften", "GeErNa"),
        ]:
            fach = Fach.objects.get(name=fach)
            przs = []
            for periode in [1, 2]:
                zeugnis = Zeugnis.objects.filter(schüler__klasse=klasse, periode = periode)

                habe = Decimal(0)
                soll = Decimal(0)
                for element in ZeugnisElement.objects.filter(kompetenz__fach=fach, abschnitt__vorlage__stufe=4):
                    if periode in overrides and element.id in overrides[periode]:
                        punkte = overrides[periode][element.id]
                    else:
                        punkte = berechne_note(element, periode, gesamtpunkte[periode], abschlussbewertungen)
                    if punkte:
                        habe += punkte
                        soll += element.max_punkte
                przs += [ prozentsatz(habe, soll) ]
            row += [csvnumber(f"{mean(przs):.03}%")]

        writer.writerow(row)

@login_required
def zeugnisse_export(request, klasse_id, as_attachment):
    klasse = Klasse.objects.get(id=klasse_id)
    filename = f"Noten {klasse}.csv"

    with open(filename, 'w', newline='') as file:
        writer = csv.writer(file, **CSV_OPTIONS)
        abschluss_csv(klasse, writer)
    print("Wrote {}".format(filename))
    fh = open(filename, 'rb')
    return FileResponse(fh, as_attachment=as_attachment, filename=filename)


@login_required
def kommentare_export(request, klasse_id, periode, as_attachment):
    from zeugnisse.docx_export import klassenrat_docx
    klasse = Klasse.objects.get(id=klasse_id)
    filename = f"Kommentare {klasse} P{periode}.docx"
    document = klassenrat_docx(klasse, periode)
    document.save(filename)
    print("Wrote {}".format(filename))
    fh = open(filename, 'rb')
    return FileResponse(fh, as_attachment=as_attachment, filename=filename)
