import io

import weasyprint
from django.contrib.auth.decorators import login_required
from django.db.models import Prefetch
from django.http import FileResponse
from django.shortcuts import get_object_or_404, redirect, render, reverse
from django.template.loader import render_to_string
from django.utils import timezone

from zeugnisse.forms import (BausteinAbschnittBewertungForm,
                             BausteinArbeitBewertungForm,
                             BausteinKommentarForm)
from zeugnisse.models import (Baustein, BausteinKommentar, LeistungsBewertung,
                              Schüler)

def baustein_print(context, schüler, abschnitte, bausteinarbeit, as_attachment):
    for ab in abschnitte:
        ab["form"].calculate_sum()
    bausteinarbeit.calculate_sum()

    summe = [
        sum(a["form"].gesamtpunkte[i] for a in abschnitte)
        + bausteinarbeit.gesamtpunkte[i]
        for i in [0, 1]
    ]
    if summe[1] > 0:
        summe += [(summe[0] / summe[1]) * 100]
    context["summe"] = summe

    html = weasyprint.HTML(string=render_to_string(
        'zeugnisse/baustein_pdf.html', context))
    css = weasyprint.CSS(filename='static/bulma/css/style.min.css')
    filename = ("Baustein-{}-{}.pdf".format(baustein, schüler))
    buffer = io.BytesIO()
    html.write_pdf(buffer, stylesheets=[css])
    buffer.seek(0)
    return FileResponse(buffer, as_attachment=as_attachment, filename=filename)

@login_required
def baustein(request, baustein_id, schüler_id):
    schüler = get_object_or_404(Schüler.objects.filter(id=schüler_id))
    baustein = get_object_or_404(
        Baustein.objects.filter(
            id=baustein_id).prefetch_related(
            'bausteinabschnitt_set',
            'bausteinabschnitt_set__leistung_set',
            'bausteinabschnitt_set__leistung_set__kompetenz__fach',
            Prefetch(
                'bausteinabschnitt_set__leistung_set__leistungsbewertung_set',
                queryset=LeistungsBewertung.objects.filter(
                    schüler=schüler),
                to_attr='vlt_bewertung')))

    abschnitte = [
        {
            "title": abschnitt.name,
            "form": BausteinAbschnittBewertungForm(
                request.POST or None,
                abschnitt,
                prefix="baustein{}".format(baustein.id)),
        }
        for abschnitt in baustein.bausteinabschnitt_set.all()
    ]

    bausteinarbeit = BausteinArbeitBewertungForm(
        request.POST or None, baustein, schüler, prefix="bausteinarbeit")

    kommentar = BausteinKommentar.objects.get_or_create(
        schüler=schüler, baustein=baustein)[0]
    kommentarform = BausteinKommentarForm(
        request.POST or None, instance=kommentar, prefix="kommentar")

    context = {
        "title": "{}: {}".format(schüler, baustein),
        'breadcrumbs': [
            ('Klasse {}'.format(schüler.klasse), reverse(
                'klasse', args=[schüler.klasse.id])),
        ],
        "baustein": baustein,
        "schüler": schüler,
        "abschnitte": abschnitte,
        "bausteinarbeit": bausteinarbeit,
        "kommentarform": kommentarform,
    }

    if (all([ab["form"].is_valid() for ab in abschnitte])
            and bausteinarbeit.is_valid()
            and kommentarform.is_valid()):
        now = timezone.now()
        for ab in abschnitte:
            ab["form"].save(request.user, schüler, now)

        bausteinarbeit.save(request.user, schüler, now)

        text = kommentarform.cleaned_data['kommentar']
        if text is not None and text.strip() != "":
            kommentarform.save()
        else:
            instance = kommentarform.save(commit=False)
            instance.delete()

        if "show" in request.POST:
            return baustein_print(context, schüler, abschnitte, bausteinarbeit, as_attachment=False)
        if "print" in request.POST:
            return baustein_print(context, schüler, abschnitte, bausteinarbeit, as_attachment=True)
        return redirect('klasse', schüler.klasse.id)

    return render(request, 'zeugnisse/baustein.html', context)
