from docx import Document
from docx.shared import Cm
from zeugnisse.models import *

def klassenrat_docx(klasse, periode):
    document = Document()
    document.add_heading(klasse.klassenleiterinnen)
    
    table = document.add_table(rows=klasse.schüler_set.count() + 1, cols=4)
    table.columns[0].width = Cm(1)
    table.columns[2].width = Cm(8)
    table.columns[3].width = Cm(5)

    table.cell(0,0).merge(table.cell(0,1))
    table.cell(0,2).add_paragraph().add_run("<Datum Einfügen>")

    for i,z in enumerate(Zeugnis.objects.filter(schüler__klasse=klasse, periode = periode).order_by("schüler")):
        schüler = z.schüler
        row = table.rows[i+1]
        row.cells[0].add_paragraph().add_run(str(i+1)).bold = True
        row.cells[1].add_paragraph().add_run(f"{z.schüler.name.upper()} {z.schüler.vorname}")
        cell = row.cells[2]
        cell.add_paragraph().add_run("Leistung:").bold = True
        for a in z.vorlage().zeugnisabschnitt_set.filter(fach__advanced=True).exclude(fach__name="Religion"):
            p = cell.add_paragraph()
            p.add_run(f"{a.fach.name}: ").underline = True
            for k in ZeugnisFachKommentar.objects.filter(schüler=schüler,periode = periode,abschnitt=a):
                p.add_run(k.kommentar)
        p = cell.add_paragraph()
        p.add_run("Verhalten: ").bold = True
        p.add_run(z.sozialverhalten_kommentar)
        for a in z.vorlage().zeugnisabschnitt_set.filter(fach__advanced=False).union(z.vorlage().zeugnisabschnitt_set.filter(fach__name="Religion")):
            p = cell.add_paragraph()
            p.add_run(f"{a.fach.name}: ").underline = True
            for k in ZeugnisFachKommentar.objects.filter(schüler=schüler,periode = periode,abschnitt=a):
                p.add_run(k.kommentar)
        cell.add_paragraph().add_run("Bemerkungen: ").bold = True

    return document

def export_all():
    for periode in [1, 2]:
        for klasse in Klasse.objects.filter(jahrgang__lte=6):
            document = klassenrat_docx(klasse, periode)
            document.save(f"Klassenrat-Export {klasse} P{periode}.docx")

