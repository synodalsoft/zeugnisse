from zeugnisse.models import *
import csv

from zeugnisse.views.zeugnis import abschluss_csv

def export(jahrgang, name):
    for klasse in Klasse.objects.filter(jahrgang=jahrgang, name=name):
        with open(f"Noten-Export 21-22_{klasse}.csv", 'w', newline='') as file:
            writer = csv.writer(file)
            abschluss_csv(klasse, writer)
