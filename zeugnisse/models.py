from enum import Enum

from django.contrib.auth.models import AbstractUser
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.db.models import CharField, ForeignKey
from django.urls import reverse_lazy
from django.utils.html import format_html
from polymorphic.models import PolymorphicModel

PERIODEN = (
    (1, '1'),
    (2, '2'),
)

JAHRGÄNGE = (
    (1, '1'),
    (2, '2'),
    (3, '3'),
    (4, '4'),
    (5, '5'),
    (6, '6'),
    (7, 'Alumni'),
)

STUFEN = (
    (1, 'Erste Klasse'),
    (2, 'Zweite Klasse'),
    (3, 'Mittelstufe'),
    (4, 'Oberstufe'),
)


class Teacher(AbstractUser):

    class Meta:
        verbose_name = 'Lehrer'
        verbose_name_plural = 'Lehrer'

    def __str__(self):
        return "{0.last_name} {0.first_name}".format(self)


class Klasse(models.Model):
    name = models.CharField(max_length=10)
    jahrgang = models.PositiveSmallIntegerField(choices=JAHRGÄNGE, default=2)
    klassenleiterinnen = models.CharField(
        max_length=50, blank=True, default="", verbose_name="KlassenleiterInnen")
    # TODO: refactor this to make it fit for all years?
    ev_religionslehrerin = models.CharField(
        max_length=50, blank=True, default="", verbose_name="LehrerIn für Evangelische Religion")
    ka_religionslehrerin = models.CharField(
        max_length=50, blank=True, default="", verbose_name="LehrerIn für Katholische Religion")
    sportlehrerin = models.CharField(
        max_length=50, blank=True, default="", verbose_name="LehrerIn für Sport")
    geographielehrerin = models.CharField(
        max_length=50, blank=True, default="", verbose_name="LehrerIn für Geographie")
    geschichtslehrerin = models.CharField(
        max_length=50, blank=True, default="", verbose_name="LehrerIn für Geschichte")
    techniklehrerin = models.CharField(
        max_length=50, blank=True, default="", verbose_name="LehrerIn für Naturwissenschaften und Technik")

    def __str__(self):
        return "{0.jahrgang}{0.name}".format(self)

    def stufe(self):
        if self.jahrgang == 1:
            return 1
        elif self.jahrgang == 2:
            return 2
        elif self.jahrgang <= 4:
            return 3
        else:
            return 4

    def get_absolute_url(self):
        return reverse_lazy('klasse', kwargs={'klasse_id': self.id})

    class Meta:
        verbose_name = 'Klasse'
        verbose_name_plural = 'Klassen'
        ordering = ['jahrgang', 'name']
        unique_together = ['name', 'jahrgang']


class Fach(models.Model):
    name = models.CharField(max_length=50, unique=True)
    advanced = models.BooleanField(
        default=True, verbose_name="Komplexes Benotungsschema (Kompetenzen, Tests, Bausteine)")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Fach'
        verbose_name_plural = 'Fächer'

class LehrerRolle(models.Model):
    lehrer = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    klasse = models.ForeignKey(Klasse, on_delete=models.CASCADE)
    fach   = models.ForeignKey(Fach, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Lehrerrolle'
        verbose_name_plural = 'Lehrerrollen'
        unique_together = ['lehrer', 'klasse', 'fach']


class Kompetenz(models.Model):
    beschreibung = CharField(max_length=300)
    fach = models.ForeignKey(
        Fach, on_delete=models.PROTECT, limit_choices_to={'advanced': True})
    mit_abschlusstests = models.BooleanField(default=False)

    def __str__(self):
        return "{}: {}".format(self.fach.name, self.beschreibung)

    class Meta:
        verbose_name = 'Kompetenz'
        verbose_name_plural = 'Kompetenzen'
        ordering = ['fach', 'beschreibung']

class VersetzungsBedingung(models.Model):
    text = models.TextField(verbose_name="Freitext", max_length=1000, default="", blank=True)
    jahr = models.PositiveSmallIntegerField(unique=True)

    class Meta:
        verbose_name = 'Versetzungsbedingung'
        verbose_name_plural = 'Versetzungsbedingungen'
        ordering = ['jahr']

class ZeugnisVorlage(models.Model):
    stufe = models.PositiveSmallIntegerField(choices=STUFEN, unique=True)

    def __str__(self):
        return "Zeugnisvorlage für die {}".format(STUFEN[self.stufe-1][1])

    class Meta:
        verbose_name = 'Zeugnisvorlage'
        verbose_name_plural = 'Zeugnisvorlagen'
        ordering = ['stufe']


class ZeugnisAbschnitt(models.Model):
    vorlage = ForeignKey(ZeugnisVorlage, on_delete=models.CASCADE)
    fach = models.ForeignKey(Fach, on_delete=models.CASCADE)
    fixtext = models.TextField(
        verbose_name="Extra-Information", max_length=1000, default="", blank=True)
    ord_outer = models.PositiveIntegerField(default=0, db_index=True)

    class Meta:
        verbose_name = 'Abschnitt'
        verbose_name_plural = 'Abschnitte'
        ordering = ['ord_outer']


class ZeugnisElement(models.Model):
    abschnitt = ForeignKey(ZeugnisAbschnitt, on_delete=models.CASCADE)
    kompetenz = models.ForeignKey(Kompetenz, on_delete=models.CASCADE)
    max_punkte = models.PositiveSmallIntegerField(default=10)

    ord_inner = models.PositiveIntegerField(default=0, db_index=True)

    class Meta:
        verbose_name = 'Kompetenz'
        verbose_name_plural = 'Kompetenzen'
        ordering = ['ord_inner']


class SozialKompetenzAbschnitt(models.Model):
    vorlage = ForeignKey(ZeugnisVorlage, on_delete=models.CASCADE)
    titel = models.CharField(max_length=100)

    ord_outer = models.PositiveIntegerField(default=0, db_index=True)

    class Meta:
        verbose_name = 'Sozialkompetenz-Abschnitt'
        verbose_name_plural = 'Sozialkompetenz-Abschnitte'
        ordering = ['ord_outer']


class SozialKompetenz(models.Model):
    abschnitt = ForeignKey(SozialKompetenzAbschnitt, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    ord_inner = models.PositiveIntegerField(default=0, db_index=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Sozialkompetenz'
        verbose_name_plural = 'Sozialkompetenzen'
        ordering = ['ord_inner']


class Schüler(models.Model):
    name = CharField(max_length=100)
    vorname = CharField(max_length=100)
    klasse = ForeignKey(Klasse, on_delete=models.PROTECT)

    def __str__(self):
        return "{0.name} {0.vorname}".format(self)

    class Meta:
        verbose_name = 'Schüler'
        verbose_name_plural = 'Schüler'
        ordering = ['name', 'vorname']

    def noten(self):
        return format_html("<a href='/u{id}'>Zur Notenübersicht</a>", id=self.id)


class LeistungsBlock(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Leistung(models.Model):
    block = models.ForeignKey(LeistungsBlock, on_delete=models.CASCADE)
    kompetenz = models.ForeignKey(Kompetenz, on_delete=models.PROTECT)
    max_punkte = models.DecimalField(
        verbose_name="Höchstpunktzahl", max_digits=4, decimal_places=1)

    def __str__(self):
        return self.kompetenz.beschreibung

    ord_inner = models.PositiveIntegerField(default=0, db_index=True)

    class Meta:
        unique_together = ('block', 'kompetenz')
        verbose_name = 'Leistung'
        verbose_name_plural = 'Leistungen'
        ordering = ['ord_inner']


def aktuelle_periode():
    return 1


class Test(LeistungsBlock):
    klasse = models.ForeignKey(Klasse, on_delete=models.PROTECT)
    periode = models.PositiveSmallIntegerField(
        choices=PERIODEN, default=aktuelle_periode)
    fach = models.ForeignKey(
        Fach, on_delete=models.PROTECT, limit_choices_to={'advanced': True})


class TestKommentar(models.Model):
    schüler = models.ForeignKey(Schüler, on_delete=models.CASCADE)
    test = models.ForeignKey(Test, on_delete=models.CASCADE)
    kommentar = models.TextField(max_length=2000)

    class Meta:
        unique_together = ('schüler', 'test')


class BausteinArbeit(models.Model):
    name = models.CharField(max_length=30, unique=True)
    max_punkte = models.PositiveSmallIntegerField(default=3)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Bausteinarbeit'
        verbose_name_plural = 'Bausteinarbeiten'


class BausteinFarbe(models.Model):
    name = models.CharField(max_length=20)
    farbe = models.CharField(max_length=7)
    priorität = models.PositiveSmallIntegerField(default=0, db_index=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Bausteinfarbe'
        verbose_name_plural = 'Bausteinfarben'
        ordering = ['priorität']

class Baustein(models.Model):
    name = models.CharField(max_length=200)
    kürzel = models.CharField(max_length=4, default="X")
    farbe = models.ForeignKey(BausteinFarbe, null=True, blank=True, on_delete=models.SET_NULL)
    jahrgang = models.PositiveSmallIntegerField(choices=JAHRGÄNGE)
    hauptkompetenz = models.ForeignKey(
        Kompetenz, on_delete=models.PROTECT, default=1)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Baustein'
        verbose_name_plural = 'Bausteine'
        ordering = ['farbe']


class BausteinAbschnitt(LeistungsBlock):
    baustein = models.ForeignKey(Baustein, on_delete=models.CASCADE)
    ord_outer = models.PositiveIntegerField(default=0, db_index=True)

    class Meta:
        verbose_name = 'Abschnitt'
        verbose_name_plural = 'Abschnitte'
        ordering = ['ord_outer']


class BausteinKommentar(models.Model):
    schüler = models.ForeignKey(Schüler, on_delete=models.CASCADE)
    baustein = models.ForeignKey(Baustein, on_delete=models.PROTECT)
    kommentar = models.TextField(max_length=2000)

    class Meta:
        unique_together = ('schüler', 'baustein')


class Bewertung(PolymorphicModel):
    # can be later than periode
    datum = models.DateTimeField(auto_now_add=True)
    periode = models.PositiveSmallIntegerField(
        choices=PERIODEN, default=aktuelle_periode)
    lehrer = models.ForeignKey(Teacher, on_delete=models.PROTECT)
    schüler = models.ForeignKey(Schüler, on_delete=models.CASCADE)

    def prozentsatz(self):
        return 100 * self.punkte / self.max_punkte()


class LeistungsBewertung(Bewertung):
    leistung = models.ForeignKey(Leistung, on_delete=models.CASCADE)
    punkte = models.DecimalField(max_digits=4, decimal_places=1)

    def max_punkte(self):
        return self.leistung.max_punkte


class BausteinArbeitBewertung(Bewertung):
    arbeit = models.ForeignKey(BausteinArbeit, on_delete=models.CASCADE)
    baustein = models.ForeignKey(Baustein, on_delete=models.CASCADE)
    punkte = models.DecimalField(max_digits=4, decimal_places=1)

    def max_punkte(self):
        return self.arbeit.max_punkte


ZEUGNISSTATUS = (
    (1, "Unfertig"),
    (2, "Zu besprechen"),
    (3, "Fertig")
)


class AbschlussTest(models.Model):
    name = models.CharField(max_length=200)
    kompetenz = models.ForeignKey(
        Kompetenz, on_delete=models.PROTECT, limit_choices_to={'mit_abschlusstests': True})
    max_punkte = models.DecimalField(
        verbose_name="Höchstpunktzahl", max_digits=4, decimal_places=1)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Abschlusstest'
        verbose_name_plural = 'Abschlusstests'
        ordering = ['name']


class AbschlussTestBewertung(Bewertung):
    test = models.ForeignKey(AbschlussTest, on_delete=models.CASCADE)
    punkte = models.DecimalField(max_digits=4, decimal_places=1)


class Zeugnis(models.Model):
    schüler = models.ForeignKey(Schüler, on_delete=models.CASCADE)
    periode = models.PositiveSmallIntegerField(choices=PERIODEN)

    status = models.PositiveSmallIntegerField(choices=ZEUGNISSTATUS, default=1)

    katholische_religion = models.BooleanField(default=True)

    sozialverhalten_kommentar = models.TextField(
        max_length=3000, blank=True, default="")

    versetzungsentscheidung = models.TextField(
        max_length=3000, blank=True, default="")

    abwesenheiten_mit_entschuldigung = models.PositiveSmallIntegerField(
        default=0)
    abwesenheiten_mit_attest = models.PositiveSmallIntegerField(default=0)
    abwesenheiten_ohne = models.PositiveSmallIntegerField(default=0)

    förderziele = models.TextField(max_length=3000, blank=True, default="")

    def vorlage(self):
        return ZeugnisVorlage.objects.get(stufe=self.schüler.klasse.stufe())

    class Meta:
        unique_together = ('schüler', 'periode')


class ZeugnisFachKommentar(models.Model):
    schüler = models.ForeignKey(Schüler, on_delete=models.CASCADE)
    periode = models.PositiveSmallIntegerField(choices=PERIODEN)

    abschnitt = models.ForeignKey(ZeugnisAbschnitt, on_delete=models.CASCADE)
    kommentar = models.TextField(max_length=3000)


class ZeugnisKompetenzNote(models.Model):
    schüler = models.ForeignKey(Schüler, on_delete=models.CASCADE)
    periode = models.PositiveSmallIntegerField(choices=PERIODEN)

    element = models.ForeignKey(ZeugnisElement, on_delete=models.CASCADE)
    note = models.DecimalField(max_digits=4, decimal_places=1)

    class Meta:
        unique_together = ('schüler', 'periode', 'element')


PRÄDIKATE = (
    (1, "Sehr gut"),
    (2, "Gut"),
    (3, "Ausreichend"),
    (4, "Mangelhaft"),
    (5, "Ungenügend")
)


class ZeugnisPrädikatNote(models.Model):
    schüler = models.ForeignKey(Schüler, on_delete=models.CASCADE)
    periode = models.PositiveSmallIntegerField(choices=PERIODEN)

    abschnitt = models.ForeignKey(ZeugnisAbschnitt, on_delete=models.CASCADE)
    note = models.PositiveSmallIntegerField(choices=PRÄDIKATE)

    class Meta:
        unique_together = ('schüler', 'periode', 'abschnitt')


ZEICHEN = (
    (4, "++"),
    (3, "+"),
    (2, "-"),
    (1, "--"),
)


class ZeugnisSozialNote(models.Model):
    schüler = models.ForeignKey(Schüler, on_delete=models.CASCADE)
    periode = models.PositiveSmallIntegerField(choices=PERIODEN)

    sozialkompetenz = models.ForeignKey(
        SozialKompetenz, on_delete=models.CASCADE)
    note = models.PositiveSmallIntegerField(choices=ZEICHEN)

    class Meta:
        unique_together = ('schüler', 'periode', 'sozialkompetenz')
