# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# symlink this to settings_local.py in development environments

SECRET_KEY = '7!%bohuol+wi6c*3%^nzr(=o_bf%w(_h31!jj^vj$9t%a%y@yu'
DEBUG = True

ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'django',
        'CONN_MAX_AGE': 5,
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

ADDITIONAL_APPS = ['silk']
ADDITIONAL_MIDDLEWARE = [
    'silk.middleware.SilkyMiddleware',
]
SILKY_PYTHON_PROFILER = True

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'



