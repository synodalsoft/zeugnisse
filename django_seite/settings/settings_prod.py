# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# symlink this to settings_local.py in the production environment

DEBUG = False

ALLOWED_HOSTS = ['pdg.bsdg.be','zeugnis.pds-heidberg.be', 'zeugnisse.mylino.net']

from os import path

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = path.dirname(path.dirname(path.abspath(__file__)))

with open(path.join(BASE_DIR, 'secret/secret_key.txt')) as f:
    SECRET_KEY = f.read().strip()

CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True


# Email Stuff
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_HOST = 'smtp.gmail.com'
# EMAIL_PORT = 465
# EMAIL_HOST_USER = 'noreply@pdg-bsdg.be'
# EMAIL_SUBJECT_PREFIX = '[Zeugnis-App] '
# EMAIL_USE_SSL = True
# EMAIL_TIMEOUT = 5
# with open(path.join(BASE_DIR, 'secret/smtp_pass.txt')) as f:
#     EMAIL_HOST_PASSWORD = f.read().strip()
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

##DATABASES = {
##    'default': {
##        # no credentials needed - the prod DB only accepts local UDS connections!
##        'ENGINE': 'django.db.backends.postgresql',
##        'NAME': 'django',
##        'CONN_MAX_AGE': 5,
##    }
##}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'django',                      
        'USER': 'django',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'file': {
            'level': 'WARNING',
            'class': 'logging.FileHandler',
            'filename': '/var/log/zeugnisse/django.log',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'WARNING',
            'propagate': True,
        },
    },
}

SILKY_PYTHON_PROFILER = False
ADDITIONAL_APPS = []
ADDITIONAL_MIDDLEWARE = []
