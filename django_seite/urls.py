"""django_seite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

from zeugnisse import views

from .settings import DEBUG

urlpatterns = [
    path('', views.index, name='index'),
    path('klasse<int:klasse_id>', views.klasse, name="klasse"),
    path('klasse<int:pk>/bearbeiten', views.KlasseBearbeiten.as_view(), name="klasse_bearbeiten"),

    path('baustein<int:baustein_id>schüler<int:schüler_id>', views.baustein, name="baustein"),

    path('klasse<int:klasse_id>fach<int:fach_id>', views.tests, name="tests"),
    path('test<int:test_id>', views.test, name="test"),
    path('klasse<int:klasse_id>fach<int:fach_id>periode<int:periode>/neuertest', views.neuertest, name="neuertest"),
    path('delete_test<int:test_id>', views.delete_test, name="delete_test"),

    path('klasse<int:klasse_id>abschlusstest<int:test_id>', views.abschlusstest, name="abschlusstest"),

    path('klasse<int:klasse_id>kompetenz<int:kompetenz_id>/nach_leistung', views.kompetenz_l, name="kompetenz_l"),
    path('klasse<int:klasse_id>kompetenz<int:kompetenz_id>/nach_schueler', views.kompetenz_s, name="kompetenz_s"),

    path('zeugnis<int:schüler_id>/<int:periode>', views.zeugnis, name="zeugnis"),
    path('klasse<int:klasse_id>zeugnisse', views.zeugnisse, name="zeugnisse"),
    path('klasse<int:klasse_id>zeugnisse/print_<int:bis_periode>', views.zeugnisse_print, {'as_attachment':True}, name="zeugnisse_print"),
    path('klasse<int:klasse_id>zeugnisse/show_<int:bis_periode>', views.zeugnisse_print, {'as_attachment':False}, name="zeugnisse_show"),
    path('klasse<int:klasse_id>zeugnisse/export', views.zeugnisse_export, {'as_attachment':True}, name="zeugnisse_export"),
    path('klasse<int:klasse_id>/kommentare/export/<int:periode>', views.kommentare_export, {'as_attachment':True}, name="kommentare_export"),

    path('admin/', admin.site.urls),
    path('grappelli/', include('grappelli.urls')),
    path('nested_admin/', include('nested_admin.urls')),
    path('', include('django.contrib.auth.urls')),
]

if DEBUG:
    urlpatterns += [path('silk/', include('silk.urls'))]
