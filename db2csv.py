"""
Export database content to csv files.

"""
import os
import django
import csv
from pathlib import Path

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "django_seite.settings")
django.setup()
from zeugnisse import models
from django.db.models import Model
from django.db.models import ForeignKey


MODEL_NAMES = """AbschlussTest
AbschlussTestBewertung
BausteinArbeitBewertung
BausteinKommentar
Bewertung
LeistungsBewertung
Test
TestKommentar
ZeugnisFachKommentar
ZeugnisKompetenzNote
ZeugnisPrädikatNote
ZeugnisSozialNote
Zeugnis
Baustein
BausteinAbschnitt
BausteinArbeit
BausteinFarbe
Fach
Klasse
Kompetenz
LehrerRolle
Leistung
LeistungsBlock
Schüler
SozialKompetenz
SozialKompetenzAbschnitt
Teacher
VersetzungsBedingung
ZeugnisVorlage
ZeugnisAbschnitt
ZeugnisElement
""".split()

def main():

    output_dir = Path("export")

    output_dir.mkdir(exist_ok=True)

    for count, mn in enumerate(MODEL_NAMES, start=1):
        m = getattr(models, mn)
        field_names = [field.name for field in m._meta.fields]
        filename = output_dir / (m.__name__ + ".csv")
        if filename.exists():
            print(f"{count}) Oops, {filename} already exists!")
            continue
        print(f"{count}) Writing {filename}...")
        with open(filename, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            # Write a first row with header information
            writer.writerow(field_names)
            # Write data rows
            for obj in m.objects.all():
                row = []
                for fld in m._meta.fields:
                    value = getattr(obj, fld.name)
                    if isinstance(value, Model):
                        value = value.pk
                    row.append(value)
                writer.writerow(row)

    print(f"Wrote {count-1} csv files.")

  # assert len(Teacher.groups)==0
  # assert Teacher.user_permissions

if __name__ == '__main__':
    main()
