"""
Zeugnisse ans neue Jahr anpassen

This script deletes most data from the database. It is run every year in August
when the academic year is archived.

"""
import os
import django
from prompt_toolkit.shortcuts import confirm

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "django_seite.settings")
django.setup()

# Werden NICHT gelöscht:
# Baustein
# BausteinAbschnitt
# BausteinArbeit
# BausteinFarbe
# Fach
# Klasse
# Kompetenz
# LehrerRolle
# Leistung
# LeistungsBlock
# Schüler
# SozialKompetenz
# SozialKompetenzAbschnitt
# Teacher
# VersetzungsBedingung
# ZeugnisVorlage
# ZeugnisAbschnitt
# ZeugnisElement


MODEL_NAMES = """AbschlussTest
AbschlussTestBewertung
BausteinArbeitBewertung
BausteinKommentar
Bewertung
LeistungsBewertung
Test
TestKommentar
ZeugnisFachKommentar
ZeugnisKompetenzNote
ZeugnisPrädikatNote
ZeugnisSozialNote
Zeugnis
""".split()

from zeugnisse import models

def main():

  for mn in MODEL_NAMES:
      m = getattr(models, mn)
      print("Delete {} {}".format(m.objects.count(), m._meta.verbose_name_plural))

  msg = "OK to remove above data from {} tables?"
  msg = msg.format(len(MODEL_NAMES))
  if not confirm(msg):
      return

  # print("Not yet implemented.")
  for mn in MODEL_NAMES:
      m = getattr(models, mn)
      m.objects.all().delete()

  print("Above data has been removed successfully.")

  # assert len(Teacher.groups)==0
  # assert Teacher.user_permissions

if __name__ == '__main__':
    main()
